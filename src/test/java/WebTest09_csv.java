import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


@RunWith(JUnitParamsRunner.class)
public class WebTest09_csv {
    WebDriver driver;


    @Test
    @FileParameters("login_data.csv")
    public void testLoggingInFromCSV(String value1, String value2, String value3) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");
        driver.findElement(By.cssSelector("#Jméno")).sendKeys(value1);
        driver.findElement(By.cssSelector("#Heslo")).sendKeys(value2);
        driver.findElement(By.cssSelector("#submit")).click();
        Assertions.assertEquals(value3, driver.getCurrentUrl());

        driver.close();
    }
}
