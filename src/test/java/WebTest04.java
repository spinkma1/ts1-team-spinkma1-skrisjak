import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.net.URL;
import java.util.Set;

public class WebTest04 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://wa.toad.cz/~spinkma1/Forum/index.php");

    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Test
    public void TestLoggedInStatusAfterClosingTheWindow() {
        driver.findElement(By.cssSelector("a[href='prihlaseni.php']")).click();
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("ADMINSOS");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("helety1");
        driver.findElement(By.cssSelector("#submit")).click();


        Set<Cookie> cookiesInstance1 = driver.manage().getCookies();
        driver.close();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.navigate().to("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");


        Set<Cookie> cookiesInstance2 = driver.manage().getCookies();

        Assertions.assertNotEquals(cookiesInstance1, cookiesInstance2);

        WebElement button = null;
        try {
            button = driver.findElement(By.cssSelector(".button"));
        } catch (NoSuchElementException ignored) {

        }
        Assertions.assertNull(button);


    }
}


