import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebTest10 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }
    @Test
    public void testDeleletingComentAsAdmin() {
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#submit")).click();
        driver.get("https://wa.toad.cz/~spinkma1/Forum/newpage_ostatni.php");
        driver.findElement(By.cssSelector(".button")).click();
        driver.findElement(By.cssSelector("#Titulek")).sendKeys("Testovaný komentář");
        driver.findElement(By.cssSelector("#Komentář")).sendKeys("Opravdu se tento komentář smaže?");
        driver.findElement(By.cssSelector("#submit")).click();
        driver.findElement(By.xpath("//a[@href='rozscetnik.php']//button[@id='toggle']//img[@id='btn_image']")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > a:nth-child(2) > span:nth-child(1)")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > form:nth-child(2) > input:nth-child(1)")).click();
        driver.findElement(By.cssSelector("tbody tr:nth-child(2) td:nth-child(6) form:nth-child(1) input:nth-child(2)")).click();
        driver.findElement(By.cssSelector("input[value='Smazat']")).click();
        try {
            WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Testovaný komentář')]"));
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(true);
        }
        driver.close();

    }
}
