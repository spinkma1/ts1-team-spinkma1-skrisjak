import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebTest03 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);

        driver.get("https://wa.toad.cz/~spinkma1/Forum/index.php");
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }
    @Test
    void changeBackgroundTheme() {
        driver.findElement(By.cssSelector("#btn_image")).click();
        driver.navigate().refresh();
        driver.findElement(By.cssSelector("a[href='onas.php']")).click();

        WebElement container = driver.findElement(By.className("container"));
        String colorValue = container.getCssValue("color");

        Assertions.assertEquals("rgba(254, 231, 21, 1)",colorValue);
    }
}
