import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginUser {
    WebDriver driver;
    @FindBy(id="Jméno")
    private WebElement jmenoinput;
    @FindBy(id="Heslo")
    private WebElement hesloinput;

    public LoginUser(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }
    public void setJmeno(String jmeno){
        this.jmenoinput .sendKeys(jmeno);
    }
    public void setHeslo(String heslo){
        this.hesloinput.sendKeys(heslo);
    }

    public void clickButton(String selector){
        driver.findElement(By.cssSelector(selector)).click();
    }

}
