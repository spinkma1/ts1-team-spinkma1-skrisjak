import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebTest06_csv {
    WebDriver driver;
    @BeforeMethod
    public void setupTest() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }
    @AfterMethod
    public void teardown() {
        driver.quit();
    }
    @DataProvider(name = "registrationData")
    public Object[][] getRegistrationData() throws CsvException, IOException {
        String csvFilePath = "testing_data.csv";
        CSVReader csvReader = new CSVReader(new FileReader(csvFilePath));
        List<String[]> rows = csvReader.readAll();
        csvReader.close();
        rows.remove(0);
        Object[][] data = new Object[rows.size()][6];
        for (int i = 0; i < rows.size(); i++) {
            String[] row = rows.get(i);
            String[] values = row[0].split(";");
            data[i][0] = values[0];
            data[i][1] = values[1];
            data[i][2] = values[2];
            data[i][3] = values[3];
            data[i][4] = values[4];
            data[i][5] = values[5];
        }
        return data;
    }
    private static boolean Validator(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    @Test(dataProvider = "registrationData")
    public void testRegistrationForm(String firstName, String password, String email, String date, String gender, String country) {
        driver.get("https://wa.toad.cz/~spinkma1/Forum/registrace.php");
        driver.findElement(By.cssSelector("#Jméno")).sendKeys(firstName);
        driver.findElement(By.cssSelector("#Heslo")).sendKeys(password);
        driver.findElement(By.cssSelector("#Email")).sendKeys(email);
        driver.findElement(By.cssSelector("#Datum")).sendKeys(date);
        driver.findElement(By.cssSelector("input[name='gender'][value="+ gender + "]")).click();
        Select select = new Select(driver.findElement(By.cssSelector("#státy")));
        select.selectByVisibleText(country);

        driver.findElement(By.cssSelector("#submit")).click();;
        LocalDate date_limit = LocalDate.now().minusYears(18);


        String url = driver.getCurrentUrl();
        if(!url.equals("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php")) {
            if (firstName.length() < 6 || firstName.length() > 15) {
                if (driver.findElement(By.cssSelector("#nameerror")).isDisplayed()) {
                    Assertions.assertEquals("Jméno je nevalidní", driver.findElement(By.cssSelector("#nameerror")).getText());
                }
            }
            if (password.length() < 7 || password.length() > 25) {
                if (driver.findElement(By.cssSelector("#passerror")).isDisplayed()) {
                    Assertions.assertEquals("Heslo je nevalidní", driver.findElement(By.cssSelector("#passerror")).getText());
                }
            }
            if (!Validator(email)) {
                if (driver.findElement(By.cssSelector("#emailerror")).isDisplayed()) {
                    Assertions.assertEquals("Email je nevalidní", driver.findElement(By.cssSelector("#emailerror")).getText());
                }
            }
            if (email.contains(".sk")) {
                if (driver.findElement(By.cssSelector("#emailerror")).isDisplayed()) {
                    Assertions.assertEquals("Slovenská emailová adresa!", driver.findElement(By.id("skerror")).getText());
                }
            }
            if (!LocalDate.now().isBefore(date_limit)) {
                System.out.println(driver.getCurrentUrl());
                if (driver.findElement(By.cssSelector("#datumerror")).isDisplayed()) {
                    Assertions.assertEquals("Datum je nevalidní", driver.findElement(By.cssSelector("#datumerror")).getText());
                }
            }
            if (gender == "Jiné") {
                if (driver.findElement(By.cssSelector("#gendererror")).isDisplayed()) {
                    Assertions.assertEquals("Nevalidní gender", driver.findElement(By.cssSelector("#gendererror")).getText());
                }
            }
            if (country == "československá") {
                if (driver.findElement(By.cssSelector("#stateerror")).isDisplayed()) {
                    Assertions.assertEquals("Nevalidní národnost", driver.findElement(By.cssSelector("#stateerror")).getText());
                }
            }
            String taken_color = driver.findElement(By.id("taken")).getCssValue("color");
            if (taken_color.equals("rgba(255, 0, 0, 1)") || taken_color.equals("red")) {
                System.out.println("User is already registered");
                driver.close();
            }

        }else{
            Assertions.assertEquals("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php",driver.getCurrentUrl());
            driver.close();
        }
    }
}


