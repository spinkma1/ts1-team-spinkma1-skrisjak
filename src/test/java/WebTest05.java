import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebTest05 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#submit")).click();
    }

    @AfterEach
    void teardown() {
        driver.findElement(By.cssSelector("#odhlasit")).click();
        driver.findElement(By.cssSelector("a[href='prihlaseni.php']")).click();
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#submit")).click();
        driver.findElement(By.xpath("//a[@href='root.php']//button[@id='toggle']//img[@id='btn_image']")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > div:nth-child(1) > form:nth-child(3) > input:nth-child(3)")).sendKeys("AdminTest");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(6));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='delete']")));
        driver.findElement(By.xpath("//input[@name='delete']")).click();





        driver.quit();
    }
    @Test
    public void changeAdminsPermissions(){
        driver.findElement(By.cssSelector("body > header:nth-child(1) > div:nth-child(1) > div:nth-child(1) > nav:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1) > button:nth-child(1) > img:nth-child(1)")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > div:nth-child(1) > form:nth-child(2) > input:nth-child(3)")).sendKeys("AdminTest");
        driver.findElement(By.xpath("//input[@name='submit']")).click();

        //LOGOUT
        driver.findElement(By.cssSelector("#odhlasit")).click();
        //LOGIN
        driver.findElement(By.cssSelector("a[href='prihlaseni.php']")).click();
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("AdminTest");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("AdminTest");
        driver.findElement(By.cssSelector("#submit")).click();

        //VERIFICATION
        try{
            driver.findElement(By.xpath("//a[@href='rozscetnik.php']//button[@id='toggle']//img[@id='btn_image']")).click();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Permission not given");
        }
        Assertions.assertEquals("https://wa.toad.cz/~spinkma1/Forum/rozscetnik.php",driver.getCurrentUrl());
        }

    }
