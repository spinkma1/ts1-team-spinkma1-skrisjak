import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.ArrayList;

public class WebTest07 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Test
    public void stillLoggedAfterClosingTab() {
        // Login
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("ADMINSOS");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("helety1");
        driver.findElement(By.cssSelector("#submit")).click();

        String textOfLoggedIn1 = driver.findElement(By.cssSelector("#login_quote")).getText();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.open()");

        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        driver.close();

        driver.switchTo().window(tabs.get(1));
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");

        String textOfLoggedIn2 = driver.findElement(By.cssSelector("#login_quote")).getText();
        Assertions.assertEquals(textOfLoggedIn1,textOfLoggedIn2);

    }
}
