import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebTest08 {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");
        driver.findElement(By.cssSelector("#Jméno")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#Heslo")).sendKeys("TEST123");
        driver.findElement(By.cssSelector("#submit")).click();


    }
    @AfterEach
    void tearUp(){
        driver.quit();
    }
    @Test
    public void changeComment() throws InterruptedException {
        driver.findElement(By.xpath("//a[@href='rozscetnik.php']//button[@id='toggle']//img[@id='btn_image']")).click();
        driver.findElement(By.id("priznakyid")).click();
        try{
            driver.findElement(By.xpath("//tbody/tr[2]/td[6]/form[1]/input[2]")).click();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("No comments to edit!");
            driver.quit();
        }
        driver.findElement(By.xpath("//input[@id='title']")).sendKeys("EDITED BY ADMIN");
        String new_title = driver.findElement(By.xpath("//input[@id='title']")).getAttribute("value");
        driver.findElement(By.xpath("//input[@name='submit']")).click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(6));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[contains(text(), '" + new_title + "')]")));




        for(int i = 2;i < 10;i++){
            String text = driver.findElement(By.xpath( "//h2[contains(text(), '" + new_title + "')]")).getText();
            Assertions.assertEquals(new_title,text);
            if(text.equals(new_title)){
                Assertions.assertEquals(new_title,text);
                driver.quit();
                break;
            }else {
                String selector = "a[href='?page=" + i + "']";
                String link = "";
                try {
                    link = driver.findElement(By.cssSelector(selector)).getText();
                } catch (NoSuchElementException e) {
                    System.out.println("Element not found: " + e.getMessage());
                    driver.quit();
                }
                try {
                    driver.findElement(By.cssSelector(link)).click();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Page not found");
                    driver.quit();
                }
            }
        }
    }

}
