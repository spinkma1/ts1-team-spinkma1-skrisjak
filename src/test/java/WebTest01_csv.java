import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class WebTest01_csv {
    WebDriver driver;


    @BeforeMethod
    void setupTest() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
    }

    @AfterMethod
    void teardown() {
        driver.quit();
    }
    @DataProvider(name = "commentsData")
    public Object[][] getCommentsData() throws IOException, CsvException {
        String csvFilePath = "comments_data.csv";
        CSVReader csvReader = new CSVReader(new FileReader(csvFilePath));
        List<String[]> rows = csvReader.readAll();
        csvReader.close();
        rows.remove(0);
        Object[][] data = new Object[rows.size()][2];
        for (int i = 0; i < rows.size(); i++) {
            String[] row = rows.get(i);
            String[] values = row[0].split(";");
            data[i][0] = values[0];
            data[i][1] = values[1];
        }
        return data;
    }
    @Test(dataProvider = "commentsData")
    public void newComment(String title, String comment) {
        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");

        LoginUser login= new LoginUser(driver);
        login.setJmeno("TEST123");
        login.setHeslo("TEST123");

        login.clickButton("#submit");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(6));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("vladnarid")));
        driver.findElement(By.id("vladnarid")).click();
        login.clickButton(".button");

        driver.findElement(By.cssSelector("#Titulek")).sendKeys(title);
        driver.findElement(By.cssSelector("#Komentář")).sendKeys(comment);;
        login.clickButton("#submit");

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[contains(text(),title)]")));
        for(int i = 2;i < 10;i++){
            String text = driver.findElement(By.xpath("//h2[contains(text(),title)]")).getText();
            if(text==title){

                Assertions.assertEquals(title,text);
                break;
            }
            String selector ="a[href='?page=" + i +"']";
            String link= "";
            try {
                link = driver.findElement(By.cssSelector(selector)).getText();
            } catch (NoSuchElementException e) {
                System.out.println("Element not found: " + e.getMessage());
                break;
            }
            login.clickButton(selector);
        }
    }
}
