import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class WebTest02 {
    WebDriver driver;
    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);


        driver.get("https://wa.toad.cz/~spinkma1/Forum/prihlaseni.php");

        LoginUser login= new LoginUser(driver);
        login.setJmeno("Tlustej Kárl");
        login.setHeslo("Houskový Knedlík");
        login.clickButton("#submit");

    }

    @AfterEach
    void teardown() {
        changePasswordBack();
        driver.quit();
    }
    void changePasswordBack(){
        driver.findElement(By.cssSelector("a[href='lost_pass.php']")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > form:nth-child(1) > div:nth-child(2) > input:nth-child(2)")).sendKeys("VýpečkySeZelim");
        driver.findElement(By.cssSelector("body > main:nth-child(2) > form:nth-child(1) > div:nth-child(3) > input:nth-child(2)")).sendKeys("Houskový Knedlík");
        driver.findElement(By.cssSelector("#Potrvdit")).sendKeys("Houskový Knedlík");

        Select sel = new Select(driver.findElement(By.cssSelector("select[name='duvod']")));
        sel.selectByVisibleText("Mám ho moc jednoduché");
        driver.findElement(By.cssSelector("input[value='3+']")).click();
        driver.findElement(By.cssSelector("#checkb")).click();
        driver.findElement(By.cssSelector("#submit")).click();

        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    @Test
    public void changePassword(){
        LoginUser login= new LoginUser(driver);
        String mainWindow = driver.getWindowHandle();
        driver.findElement(By.cssSelector("a[href='lost_pass.php']")).click();
        driver.findElement(By.cssSelector("body > main:nth-child(2) > form:nth-child(1) > div:nth-child(2) > input:nth-child(2)")).sendKeys("Houskový Knedlík");
        driver.findElement(By.cssSelector("body > main:nth-child(2) > form:nth-child(1) > div:nth-child(3) > input:nth-child(2)")).sendKeys("VýpečkySeZelim");
        driver.findElement(By.cssSelector("#Potrvdit")).sendKeys("VýpečkySeZelim");

        Select sel = new Select(driver.findElement(By.cssSelector("select[name='duvod']")));
        sel.selectByVisibleText("Mám ho moc jednoduché");
        driver.findElement(By.cssSelector("input[value='3+']")).click();
        driver.findElement(By.cssSelector("#checkb")).click();
        driver.findElement(By.cssSelector("#submit")).click();

        Alert alert = driver.switchTo().alert();
        alert.accept();

        driver.switchTo().window(mainWindow);

        driver.findElement(By.cssSelector("#odhlasit")).click();
        driver.findElement(By.cssSelector("a[href='prihlaseni.php']")).click();

        login.setJmeno("Tlustej Kárl");
        login.setHeslo("VýpečkySeZelim");
        login.clickButton("#submit");

        driver.findElement(By.cssSelector("a[href='lost_pass.php']"));
        Assertions.assertEquals("https://wa.toad.cz/~spinkma1/Forum/index.php",driver.getCurrentUrl());






}}
